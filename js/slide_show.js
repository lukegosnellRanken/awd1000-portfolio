$(document).ready(function(){
  $('#slides').slick({
    dots: false,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000
  });
});
