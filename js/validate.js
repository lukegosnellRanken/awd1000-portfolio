//	Declare and initialize global program constants

//	Declare and initialize global program variables
var isValid = true;		//	Boolean form validation success flag
var regexpattern = "";		//	Holds current Regular Expression (regex)pattern
var string = "";			//	String to hold final (valid) results

$(document).ready(function() {
  $("validate").onclick		=	validateForm;
	$("clear").onclick			=	clearAll;
	$("ssn").focus();

  var validateForm = function ()
  {
  	//	Copy values from form inputs
  	//	into local JavaScript variables
  	var ssn			= $("ssn").val();
  	var fname 		= $("fname").val();
  	var lname		= $("lname").val();
  	var address		= $("address").val();
  	var city			= $("city").val();
  	var state			= $("state").val();
  	var zip			= $("zip").val();
  	var cellphone		= $("cellphone").val();
  	var gender;
    var age;
    var reason;

  	//alert ("Inputted password was: " + password + "\n" +
  	//	   "Inputted confirm  was: " + confirmpassword);
  /*	var birthdate		= $("birthDate").value;
  	var favoritecolor	= $("favoritecolor").value;
  	var personalwebsite	= $("personalwebsite").value;
  	var ccnumber		= $("ccnumber").value;
  */
  	//	Validate Social Security Number Field
  	validateSSN(ssn);
  	validateFirstName(fname);
  	validateLastName(lname);
  	validateAddress(address);
  	validateCity(city);
  	validateState(state);
  	validateZip(zip);
  	validateCellPhone(cellphone);
  	validateGender();
  	validateAge();
  	validateReason();

  function validateSSN(s)
  {
  	if (s == "")
  	{
  		$("ssn_error").firstChild.nodeValue =
  						"SSN Required.";
  		isValid = false;
  	}
  	else
  	{
  		regexpattern = /^\d{3}-\d{2}-\d{4}$/;

  		if (!s.match(regexpattern))
  		{
  			$("ssn_error").firstChild.nodeValue =
  						"SSN Must Be In nnn-nn-nnnn Format.";
  			isValid = false;
  		}
  		else
  		{
  			$("ssn_error").firstChild.nodeValue = "";
  			string += "Social Security Number:  " + s + "\n";
  		}
  	}
  }

  function validateFirstName(f)
  {
  	// Validate the First Name
  	if (f.trim() == "")
  	{
  		$("fname_error").firstChild.nodeValue =
  						"First Name Required.";
  		isValid = false;
  	}
  	else
  	{
  		regexpattern = /^([A-Za-z]){1,50}$/;

  		if (!f.match(regexpattern))
  		{
  			$("fname_error").firstChild.nodeValue =
  						"First Name Must Be Alpha Only.";
  			isValid = false;
  		}
  		else
  		{
  			$("fname_error").firstChild.nodeValue = "";
  			string += "First Name:  " + f + "\n";
  		}
  	}
  }

  function validateLastName(l)
  {
  	// Validate the Last Name
  	if (l.trim() == "")
  	{
  		$("lname_error").firstChild.nodeValue =
  						"Last Name Required.";
  		isValid = false;
  	}
  	else
  	{
  		regexpattern = /^([A-Za-z]){1,50}$/;

  		if (!l.match(regexpattern))
  		{
  			$("lname_error").firstChild.nodeValue =
  						"Last Name Must Be Alpha Only.";
  			isValid = false;
  		}
  		else
  		{
  			$("lname_error").firstChild.nodeValue = "";
  			string += "Last Name:  " + l + "\n";
  		}
  	}
  }

  function validateAddress(a)
  {
  	// Validate the Address
  	if (a.trim() == "")
  	{
  		$("address_error").firstChild.nodeValue =
  						"Address Required.";
  		isValid = false;
  	}
  	else
  	{
  		$("address_error").firstChild.nodeValue = "";
  		string += "Address:  " + a + "\n";
  	}
  }

  function validateCity(c)
  {
  	// Validate the City
  	if (c.trim() == "")
  	{
  		$("city_error").firstChild.nodeValue =
  						"City Required.";
  		isValid = false;
  	}
  	else
  	{
  		$("city_error").firstChild.nodeValue = "";
  		string += "City:  " + c + "\n";
  	}
  }

  function validateState(s)
  {
  	// Validate the Address
  	if ((s == "Please Select Your State:") || (s == ""))
  	{
  		$("state_error").firstChild.nodeValue =
  						"State Name Required.";
  		isValid = false;
  	}
  	else
  	{
  		$("state_error").firstChild.nodeValue = "";
  		string += "State:  " + s + "\n";
  	}
  }

  function validateZip(z)
  {
  	// Validate the Zip Code
  	if (z.trim() == "")
  	{
  		$("zip_error").firstChild.nodeValue =
  						"Zip Code Required.";
  		isValid = false;
  	}
  	else
  	{
  		regexpattern = /^\d{5}([\-]\d{4})?$/;

  		if (!z.match(regexpattern))
  		{
  			$("zip_error").firstChild.nodeValue =
  						"Zip Code nnnnn or nnnnn-nnnn only.";
  			isValid = false;
  		}
  		else
  		{
  			$("zip_error").firstChild.nodeValue = "";
  			string += "Zip Code: " + z + "\n";
  		}
  	}
  }

  function validateCellPhone(c)
  {
  	// Validate the Phone Number
  	if (c.trim() == "")
  	{
  		$("cellphone_error").firstChild.nodeValue =
  						"Phone Number Required.";
  		isValid = false;
  	}
  	else
  	{
  		regexpattern = /^\d{3}-\d{3}-\d{4}$/;

  		if (!c.match(regexpattern))
  		{
  			isValid = false;
  			$("cellphone_error").firstChild.nodeValue =
  						"# must be in nnn-nnn-nnnn format.";
  		}
  		else
  		{
  			$("cellphone_error").firstChild.nodeValue = "";
  			string += "Phone Number:  " + c + "\n";
  		}
  	}
  }

  function validateGender()
  {
  	var els		= document.getElementsByClassName('g');
  	var count		= 0;

  //	alert("The value of els in the validateShift() function is: " + els.length)
  	for (var i = 0; i < els.length; ++i)
  	{
  		if (els[i].checked)
  		{
  			++count;
  			break;
  		}
  	}

  //	alert("Count = " + count);
  	if (count === 0)
  	{
  		$("gender_error").firstChild.nodeValue = "Gender Required.";
  		isValid = false;
  	}
  	else
  	{
  		var s = document.querySelector('input[name = "gender"]:checked').val();
  //		alert ("The checked one is: " + s);
  		$("gender_error").firstChild.nodeValue = "";
  		string += "Gender:  " +
  				 s.charAt(0).toUpperCase() + s.slice(1) + "\n";
  	}
  }

  function validateAge()
  {
  	var inputElems	= document.getElementsByTagName("input");
      var count 	= 0;
  	var choices = "";

      for (var i = 0; i < inputElems.length; ++i)
  	{
  		if (inputElems[i].type === "checkbox" && inputElems[i].checked === true)
  		{
  			$("age_error").firstChild.nodeValue = "";
  //			alert ("The value of inputElems[i].value is " + inputElems[i].value)
  			choices += inputElems[i].val() + " ";
  			++count;
  		}
  	}

  	if (count == 0)
  	{
  		$("age_error").firstChild.nodeValue =
  					"Contact Via Required.";
  		isValid = false;
  	}
  	else
  	{
  		string += "Contact Via:  " + choices + "\n";
  	}
  }

  function validateReason()
  {
  	var els		= document.getElementsByClassName('reason');
  	var count		= 0;

  //	alert("The value of els in the validateEducation() function is: " + els.length)
  	for (var i = 0; i < els.length; ++i)
  	{
  		if (els[i].checked)
  		{
  			++count;
  			break;
  		}
  	}

  //	alert("Count = " + count);
  	if (count === 0)
  	{
  		$("reason_error").firstChild.nodeValue = "Highest Education Required.";
  		isValid = false;
  	}
  	else
  	{
  		var e = document.querySelector('input[name = "education"]:checked').val();
  //		alert ("The checked one is: " + e);
  		$("reason_error").firstChild.nodeValue = "";
  		string += "Highest Education:  " +
  				 e.charAt(0).toUpperCase() + e.slice(1) + "\n";
  	}
  }
});

var clearAll = function() {
	$("ssn").val()				=	"";
	$("ssn_error").val()			=	"*";
	$("fname").val()				=	"";
	$("fname_error").val()		=	"*";
	$("lname").val()				=	"";
	$("lname_error").val()		=	"*";
	$("address").val()			=	"";
	$("address_error").val()		=	"*";
	$("city").val()				=	"";
	$("city_error").val()			=	"*";
	$("state").val()				=	"";
	$("state_error").val()		=	"*";
	$("zip").val()				=	"";
	$("zip_error").val()			=	"*";
	$("cellphone").val()			=	"";
	$("cellphone_error").val()	=	"*";
	$("gender").val()				=	"";
	$("gender_error").val() 		=	"*";
	$("age").val()			=	"";
	$("age_error").val()	=	"*";
	$("reason").val() 			=	"";
	$("reason_error").val()	=	"*";
	$("ssn").focus();
}
